package com.tunmii.springbootpractice.controller;

import com.tunmii.springbootpractice.dto.ManufactureDTO;
import com.tunmii.springbootpractice.entity.Manufacture;
import com.tunmii.springbootpractice.service.ManufactureService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1/api/manufactures")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@PreAuthorize("hasRole('ROLE_USER')")
public class ManufactureController {

    private ManufactureService manufactureService;

    @GetMapping()
    public ResponseEntity<Page<ManufactureDTO>> getAllManufactures(Pageable pageable) {
        Page<ManufactureDTO> page = manufactureService.getAllManufactures(pageable);
        return ResponseEntity.ok(page);
    }

    @PostMapping()
    public ResponseEntity<ManufactureDTO> addManufacture(@RequestBody Manufacture manufacture) {
        ManufactureDTO manufactureDTO = manufactureService.addManufacture(manufacture);
        return ResponseEntity.ok(manufactureDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ManufactureDTO> updateManufacture(@PathVariable Long id,
                                            @RequestBody Manufacture manufacture){
        manufacture.setId(id);
        ManufactureDTO manufactureDTO = manufactureService.addManufacture(manufacture);
        return ResponseEntity.ok(manufactureDTO);
    }

    @DeleteMapping("/{id}")
    public void deleteManufacture(@PathVariable Long id) {
        manufactureService.deleteManufactureById(id);
    }
}
