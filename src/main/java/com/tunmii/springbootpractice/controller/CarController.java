package com.tunmii.springbootpractice.controller;

import com.tunmii.springbootpractice.dto.CarDTO;
import com.tunmii.springbootpractice.entity.Manufacture;
import com.tunmii.springbootpractice.request.CarRequest;
import com.tunmii.springbootpractice.service.CarService;
import com.tunmii.springbootpractice.service.ManufactureService;
import jakarta.validation.Valid;
import java.rmi.NotBoundException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/api/cars")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class CarController {

    private CarService carService;
    private ManufactureService manufactureService;

    @GetMapping()
    public Page<CarDTO> getAllCar(Pageable pageable, CarRequest carRequest) {
        return carService.filterCarList(pageable, carRequest);
    }

    @PostMapping()
    public ResponseEntity<CarDTO> addCar(@Valid @RequestBody CarRequest carRequest)
            throws Exception {
        Manufacture manufacture = manufactureService.getManufactureById(
                carRequest.getManufactureId());
        carService.testTransactionalWithException(carRequest, manufacture);
        return ResponseEntity.ok(carService.insertCar(carRequest, manufacture));
    }

    @PutMapping("/{id}")
    public ResponseEntity<CarDTO> updateCar(@PathVariable Long id,
            @RequestBody CarRequest carRequest) throws NotBoundException {
        CarDTO carDTO = carService.getCarById(id);
        if (carDTO == null) {
            return ResponseEntity.notFound().build();
        }

        Manufacture manufacture = manufactureService.getManufactureById(
                carRequest.getManufactureId());
        return ResponseEntity.ok(carService.updateCar(id, carRequest, manufacture));
    }

    @DeleteMapping("/{id}")
    public void deleteCar(@PathVariable Long id) {
        carService.deleteCarById(id);
    }
}
