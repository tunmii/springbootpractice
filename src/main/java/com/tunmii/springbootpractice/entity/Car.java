package com.tunmii.springbootpractice.entity;

import com.tunmii.springbootpractice.commons.CarModel;
import com.tunmii.springbootpractice.commons.ModelConvertor;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "car_sequence")
    @SequenceGenerator(name = "car_sequence",
            sequenceName = "car_seq",
            allocationSize = 1,
            initialValue = 12)
    private Long id;

    private String name;

    @Convert(converter = ModelConvertor.class)
    @Enumerated(EnumType.STRING)
    private CarModel model;

    private LocalDateTime buyDate;

    @ManyToOne
    private Manufacture manufacture;
}
