package com.tunmii.springbootpractice.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Getter
@Setter
public class Manufacture {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "manufacture_sequence")
    @SequenceGenerator(name = "manufacture_sequence", sequenceName = "manufacture_seq", allocationSize = 1, initialValue = 4)
    private Long id;
    private String name;
    private String address;

    @OneToMany(mappedBy = "manufacture")
    private Set<Car> cars;
}
