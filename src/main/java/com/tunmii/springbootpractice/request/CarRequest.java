package com.tunmii.springbootpractice.request;

import com.querydsl.core.BooleanBuilder;
import com.tunmii.springbootpractice.commons.PredicateBooleanExpression;
import com.tunmii.springbootpractice.entity.QCar;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class CarRequest {

    private Long id;
    @NotBlank
    @Size(min = 2, max = 256)
    private String name;
    @NotBlank
    private String model;
    @NotNull
    private LocalDateTime buyDate;

    private Long manufactureId;
    private List<String> manufactureName;

    public BooleanBuilder getFilterBuilder() {
        QCar car = QCar.car;
        PredicateBooleanExpression predicateBooleanExpression = new PredicateBooleanExpression();
        return predicateBooleanExpression.notNullAndLike(car.name, this.name)
                .notNullAndLike(car.model.stringValue(), this.model)
                .notNullAndEqual(car.manufacture.id, this.manufactureId)
                .notNullAndIn(car.manufacture.name, this.manufactureName)
                .getBooleanBuilder();
    }
}
