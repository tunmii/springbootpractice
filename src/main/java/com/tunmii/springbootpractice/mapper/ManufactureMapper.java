package com.tunmii.springbootpractice.mapper;

import com.tunmii.springbootpractice.dto.ManufactureDTO;
import com.tunmii.springbootpractice.entity.Manufacture;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "Spring")
public interface ManufactureMapper {
    ManufactureMapper INSTANCE = Mappers.getMapper(ManufactureMapper.class);

    ManufactureDTO manufactureToManufactureDTO(Manufacture manufacture);
}
