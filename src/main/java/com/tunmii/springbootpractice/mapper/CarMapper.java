package com.tunmii.springbootpractice.mapper;

import com.tunmii.springbootpractice.dto.CarDTO;
import com.tunmii.springbootpractice.entity.Car;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CarMapper {
    CarMapper INSTANCE = Mappers.getMapper(CarMapper.class);

    CarDTO carToCarDTO(Car car);
}
