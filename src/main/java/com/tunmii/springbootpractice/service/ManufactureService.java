package com.tunmii.springbootpractice.service;

import static com.tunmii.springbootpractice.mapper.ManufactureMapper.INSTANCE;

import com.tunmii.springbootpractice.dto.ManufactureDTO;
import com.tunmii.springbootpractice.entity.Manufacture;
import com.tunmii.springbootpractice.repository.ManufactureRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ManufactureService {
    private final ManufactureRepository manufactureRepository;

    public Manufacture getManufactureById(Long id) {
        return manufactureRepository.findById(id).orElse(null);
    }

    public Page<ManufactureDTO> getAllManufactures(Pageable pageable) {
        return manufactureRepository.findAll(pageable)
                .map(INSTANCE::manufactureToManufactureDTO);
    }

    public ManufactureDTO addManufacture(Manufacture manufacture){
        return INSTANCE.manufactureToManufactureDTO(
                manufactureRepository.save(manufacture));
    }

    public void deleteManufactureById(Long id) {
        manufactureRepository.deleteById(id);
    }
}
