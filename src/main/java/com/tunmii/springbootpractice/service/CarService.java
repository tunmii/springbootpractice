package com.tunmii.springbootpractice.service;

import static com.tunmii.springbootpractice.mapper.CarMapper.INSTANCE;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.tunmii.springbootpractice.commons.CarModel;
import com.tunmii.springbootpractice.dto.CarDTO;
import com.tunmii.springbootpractice.entity.Car;
import com.tunmii.springbootpractice.entity.Manufacture;
import com.tunmii.springbootpractice.repository.CarRepository;
import com.tunmii.springbootpractice.request.CarRequest;
import java.rmi.NotBoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CarService {
    private final CarRepository carRepository;

    public CarDTO getCarById(Long id) throws NotBoundException {
        Optional<Car> carOptional = carRepository.findById(id);
        return carOptional.map(INSTANCE::carToCarDTO).orElseThrow(NotBoundException::new);
    }

    public Page<CarDTO> getAllCar(Pageable pageable){
        return carRepository.findAll(pageable).map(INSTANCE::carToCarDTO);
    }

    public CarDTO insertCar(CarRequest carRequest, Manufacture manufacture){
        Car car = new Car();
        car.setName(carRequest.getName());
        car.setModel(CarModel.valueOf(carRequest.getModel()));
        car.setBuyDate(carRequest.getBuyDate());
        car.setManufacture(manufacture);
        return INSTANCE.carToCarDTO(carRepository.save(car));
    }

    public CarDTO updateCar(Long carId, CarRequest carRequest, Manufacture manufacture){
        Car car = new Car();
        car.setId(carId);
        car.setName(carRequest.getName());
        car.setModel(CarModel.valueOf(carRequest.getModel()));
        car.setBuyDate(carRequest.getBuyDate());
        car.setManufacture(manufacture);
        return INSTANCE.carToCarDTO(carRepository.save(car));
    }

    public void deleteCarById(Long id){
        carRepository.deleteById(id);
    }

    public Page<CarDTO> filterCarList(Pageable pageable, CarRequest carRequest){
        Predicate booleanBuilder = carRequest.getFilterBuilder();
        return carRepository.findAll(booleanBuilder, pageable).map(INSTANCE::carToCarDTO);
    }

    @Transactional
    public void testTransactionalWithException(CarRequest carRequest, Manufacture manufacture)
            throws Exception {
        Car car = new Car();
        car.setName(carRequest.getName());
        car.setModel(CarModel.valueOf(carRequest.getModel()));
        car.setBuyDate(carRequest.getBuyDate());
        car.setManufacture(manufacture);
        carRepository.save(car);
        throw new Exception("transactional exception");
    }
}
