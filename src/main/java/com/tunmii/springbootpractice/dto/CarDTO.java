package com.tunmii.springbootpractice.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CarDTO {
    private Long id;
    private String name;
    private String model;
    private LocalDateTime buyDate;
    private ManufactureDTO manufacture;
}
