package com.tunmii.springbootpractice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ManufactureDTO {
    private Long id;
    private String name;
    private String address;
}
