package com.tunmii.springbootpractice.repository;

import com.tunmii.springbootpractice.entity.Manufacture;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManufactureRepository extends JpaRepository<Manufacture, Long> {
}
