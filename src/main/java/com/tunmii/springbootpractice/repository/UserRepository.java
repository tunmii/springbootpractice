package com.tunmii.springbootpractice.repository;

import com.tunmii.springbootpractice.entity.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface UserRepository extends JpaRepository<User,Long>, QuerydslPredicateExecutor<User> {
    Optional<User> findByUsername(String username);
}
