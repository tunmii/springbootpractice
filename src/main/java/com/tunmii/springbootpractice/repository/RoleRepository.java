package com.tunmii.springbootpractice.repository;

import com.tunmii.springbootpractice.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface RoleRepository extends JpaRepository<Role, Long>, QuerydslPredicateExecutor<Role> {
    Role findByName(String name);
}
