package com.tunmii.springbootpractice.commons;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.SimpleExpression;
import com.querydsl.core.types.dsl.StringExpression;
import lombok.Getter;

import java.util.List;

@Getter
public class PredicateBooleanExpression {

    private final BooleanBuilder booleanBuilder;

    public PredicateBooleanExpression() {
        this.booleanBuilder = new BooleanBuilder();
    }

    public PredicateBooleanExpression(BooleanBuilder booleanBuilder) {
        this.booleanBuilder = booleanBuilder;
    }

    public <T> PredicateBooleanExpression notNullAndEqual(SimpleExpression<T> expression, T value) {
        if (value != null) {
            this.booleanBuilder.and(expression.eq(value));
        }
        return this;
    }

    public PredicateBooleanExpression notNullAndLike(StringExpression expression, String value) {
        if (value != null) {
            this.booleanBuilder.and(expression.contains(value));
        }
        return this;
    }

    public <T> PredicateBooleanExpression notNullAndIn(SimpleExpression<T> listExpression,
            List<T> list) {
        if (list != null && !list.isEmpty()) {
            this.booleanBuilder.and(listExpression.in(list));
        }
        return this;
    }

    public <T> PredicateBooleanExpression notNullOrEqual(SimpleExpression<T> expression, T value) {
        if (value != null) {
            this.booleanBuilder.or(expression.eq(value));
        }
        return this;
    }

}
