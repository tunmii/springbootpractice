package com.tunmii.springbootpractice.commons;

public enum CarModel {
    VIOS,
    CITY,
    HOMETOWN,
    MOUNTAIN;

}
