package com.tunmii.springbootpractice.commons;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class ModelConvertor implements AttributeConverter<CarModel, String> {

    @Override
    public String convertToDatabaseColumn(CarModel carModel) {
        if (carModel == null){
            return null;
        }
        return carModel.name();
    }

    @Override
    public CarModel convertToEntityAttribute(String s) {
        if (s == null){
            return null;
        }
        try{
            return CarModel.valueOf(s);
        }catch(IllegalArgumentException e) {
            return null;
        }
    }
}
