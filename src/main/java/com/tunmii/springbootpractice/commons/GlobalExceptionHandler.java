package com.tunmii.springbootpractice.commons;

import io.jsonwebtoken.ExpiredJwtException;
import io.swagger.v3.oas.models.responses.ApiResponse;
import java.rmi.NotBoundException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiError> invalidRequest(MethodArgumentNotValidException ex) {
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        Map<String, String> errors = fieldErrors.stream()
                .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
        return ResponseEntity.status(400)
                .body(new ApiError("Error in " + ex.getObjectName(), errors, 400));
    }


    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ApiError> internalServerError(DataIntegrityViolationException ex) {
        return ResponseEntity.status(400)
                .body(new ApiError("Error constraint data in the database", 400));
    }

    @ExceptionHandler(ExpiredJwtException.class)
    public ResponseEntity<ApiError> expiredToken(ExpiredJwtException ex) {
        return ResponseEntity.status(401)
                .body(new ApiError("Expired token", 404));
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ApiError> illegalArgument(IllegalArgumentException ex) {
        return ResponseEntity.status(404)
                .body(new ApiError(ex.getMessage(), 404));
    }

    @ExceptionHandler(NotBoundException.class)
    public ResponseEntity<ApiError> outOfBounds(NotBoundException ex) {
        return ResponseEntity.status(404)
                .body(new ApiError("ID out of index", 404));
    }
}
