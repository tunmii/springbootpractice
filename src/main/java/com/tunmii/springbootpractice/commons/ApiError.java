package com.tunmii.springbootpractice.commons;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class ApiError {
    private String message;
    private Map<String, String> messages;
    private int status;

    public ApiError(String message, int status){
        this.message = message;
        this.status = status;
    }

    public ApiError(Map<String, String> messages, int status){
        this.messages = messages;
        this.status = status;
    }

    public ApiError(String message, Map<String, String> messages, int status){
        this.message = message;
        this.messages = messages;
        this.status = status;
    }
}
