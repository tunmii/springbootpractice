package com.tunmii.springbootpractice.commons;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Slf4j
public class ApiActivityAspect {
    @Pointcut("within(com.tunmii.springbootpractice.controller.*)")
    public void controllerMethod(){};

    public String getExecutionName(JoinPoint joinPoint){
        return joinPoint.getSignature().getName();
    }

    @Before("controllerMethod()")
    public void beforeMethodExecution(JoinPoint joinPoint){
        log.info("Starting execution of {}", getExecutionName(joinPoint));
    }

    @After("controllerMethod()")
    public void afterMethodExecution(JoinPoint joinPoint){
        log.info("Ending execution of {}", getExecutionName(joinPoint));
    }
}
