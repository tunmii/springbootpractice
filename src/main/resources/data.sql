insert into manufacture (id, name, address) values (1, 'Huynhdai', '123, Hai Ba Trung'),
                                          (2, 'Ford', '456, Nguyen Thi Minh Khai'),
                                          (3, 'Toyota', '789 Ba Huyen Thanh Quan');

insert into car (id, name, model, buy_date, manufacture_id) values (1, 'Car 1', 'VIOS', '2024-01-07', 1),
                                                                   (2, 'Car 2', 'CITY', '2024-01-07', 2),
                                                                   (3, 'Car 3', 'HOMETOWN', '2024-01-07', 2),
                                                                   (4, 'Car 4', 'MOUNTAIN', '2024-01-07', 3),
                                                                   (5, 'Car 5', 'HOMETOWN', '2024-01-07', 1),
                                                                   (6, 'Car 6', 'CITY', '2024-01-07', 1),
                                                                   (7, 'Car 7', 'VIOS', '2024-01-07', 3),
                                                                   (8, 'Car 8', 'CITY', '2024-01-07', 3),
                                                                   (9, 'Car 9', 'CITY', '2024-01-07', 2),
                                                                   (10, 'Car 10', 'VIOS', '2024-01-07', 1),
                                                                   (11, 'Car 11', 'MOUNTAIN', '2024-01-07', 2);

INSERT INTO users(id, username, password) VALUES
                        (1,'ramesh','$2a$10$5PiyN0MsG0y886d8xWXtwuLXK0Y7zZwcN5xm82b4oDSVr7yF0O6em'),
                        (2,'admin','$2a$10$gqHrslMttQWSsDSVRTK1OehkkBiXsJ/a4z2OURU./dizwOQu5Lovu');

INSERT INTO roles(id, name) VALUES (1,'ROLE_ADMIN'),(2,'ROLE_USER');

INSERT INTO users_roles VALUES (2,1),(1,2);